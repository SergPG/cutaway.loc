<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use  App\Models\Portfolio;

class HomeController extends Controller
{
    //
    public function index(){

    	$portfolios = Portfolio::all();
       // dd($portfolios);
    	
    	return view('layouts.cutaway')->with([
    			'portfolios' => $portfolios
    	]);
    }
}
