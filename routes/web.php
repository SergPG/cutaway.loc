<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
D:\laragon\www\cutaway.loc\resources\views\layouts
|
*/

Route::get('/', 'Frontend\HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home1');
